package tresenderecos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import lexico.*;
import sintatico.AnaliseSintatica;


/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class TresEnderecos {

    public static ArrayList<Dado> tabelaTokensCertos = new ArrayList();
    static ArrayList<Simbolo> tabelaSimbolos = new ArrayList();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFileChooser fc = new JFileChooser();
        if (fc.showOpenDialog(fc) == 0) {
            try (FileReader ponteiroArquivo = new FileReader(fc.getSelectedFile().getAbsolutePath())) {

                try (BufferedReader codigo = new BufferedReader(ponteiroArquivo);) {
                    new AnaliseLexica(codigo, tabelaTokensCertos, tabelaSimbolos);
                    new AnaliseSintatica(tabelaTokensCertos, tabelaSimbolos);
                }

                JOptionPane.showMessageDialog(null, "Executado com sucesso!", "Resultado Analise Sintatica",INFORMATION_MESSAGE);

            } catch (IOException ex) {
                System.out.print(ex);
            }
        }
    }
    
}

package tresenderecos;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lexico.*;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class GerarCodigo {

    static ArrayList<String> todoCodigo = new ArrayList<>();

    public void GeradorCodigo(String codigo){
        codigo = codigo + " ";
        todoCodigo.add(codigo);
    }
    /**
     *
     * @param pilha
     */
    public void ArrumarCodigo(ArrayList<Token> pilha) {
        String Codigo = "";
        String linha;
        for(Token aux: pilha) {
            if("id".equals(aux.getTipo()) || "reserved_word".equals(aux.getTipo())) {
                linha = aux.getValor();
                Codigo = ("t") + (linha);
            }
            else {
                linha = aux.getValor();
                Codigo = Codigo + (linha);
            }
        }
        System.out.println(Codigo);
        Imprimir(Codigo);
    }

    
    private void Imprimir (String Codigo) {
        try (BufferedWriter arquivo = new BufferedWriter(new FileWriter("Codigo.txt"))) {
            String linha;

            linha = "CÓDIGO TRÊS ENDEREĆOS\nresultado = op1 operador op2\n";
            arquivo.append(linha);
            linha = Codigo + "\n";
            arquivo.append(linha);
        }
        catch (IOException ex) {
            Logger.getLogger(GerarCodigo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public GerarCodigo(ArrayList<Simbolo> tabelaSimbolos) {
            BufferedWriter arquivo;
        try {
            arquivo = new BufferedWriter(new FileWriter("TodoCodigo.txt"));

            String linha;

            if (todoCodigo.isEmpty()) {
                linha = "\n\n NENHUM CODIGO GERADO\n";
                arquivo.append(linha);
            } else {
                linha = "\n\n CODIGO(S)\n";
                arquivo.append(linha);
                for (String dado : todoCodigo) {
                    linha = dado + "\n";
                    arquivo.append(linha);
                }
            }


            linha = "\n\nTABELA DE SIMBOLOS\n"
                    + "Indice\tLexema\n";
            arquivo.append(linha);
            for (Simbolo simbolo : tabelaSimbolos) {
                linha = simbolo.getIndice() + "\t" + simbolo.getLexema() + "\n";
                arquivo.append(linha);
            }
            arquivo.close();
        }
        catch (IOException ex) {
            Logger.getLogger(GerarCodigo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

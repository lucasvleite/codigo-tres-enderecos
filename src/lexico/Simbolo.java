package lexico;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class Simbolo {
    private int indice;
    private String lexema;

    public Simbolo(int indice, String lexema) {
        this.indice = indice;
        this.lexema = lexema;
    }

    public int getIndice() {
        return indice;
    }

    public void setIndice(int indice) {
        this.indice = indice;
    }

    public String getLexema() {
        return lexema;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }
}
package lexico;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class Dado {
    private long linha;
    private long coluna;
    private String lexema;
    private Token token;

    public Dado(long linha, long coluna) {
        this.linha = linha;
        this.coluna = coluna;
        this.lexema = null;
        this.token = null;
    }
    
    public Dado(long linha, long coluna, String lexema, Token token) {
        this.linha = linha;
        this.coluna = coluna;
        this.lexema = lexema;
        this.token = token;
    }

    public long getLinha() {
        return linha;
    }

    public void setLinha(long linha) {
        this.linha = linha;
    }

    public long getColuna() {
        return coluna;
    }

    public void setColuna(long coluna) {
        this.coluna = coluna;
    }

    public String getLexema() {
        return lexema;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
    
}
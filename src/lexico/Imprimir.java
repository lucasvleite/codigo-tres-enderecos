package lexico;



import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class Imprimir {

    Imprimir(ArrayList<Simbolo> tabelaSimbolos, ArrayList<Dado> tabelaDadosCertos,
            ArrayList<Dado> tabelaDadosErrados) {

        try {
            BufferedWriter arquivoTokens = new BufferedWriter(new FileWriter("Tokens.csv"));
            BufferedWriter arquivoTabelas = new BufferedWriter(new FileWriter("Tabela.csv"));

            String linha;

            linha = "Tokens: < tipo , valor >\n";
            arquivoTokens.append(linha);
            linha = "Linha\tColuna\tLexema\tTipo Token\n";
            arquivoTabelas.append(linha);

            int count = 1;
            for (Dado dado : tabelaDadosCertos) {
                if (count % 5 == 0) {
                    linha = dado.getToken().getStringToken() + "\n";
                    arquivoTokens.append(linha);
                } else {
                    linha = dado.getToken().getStringToken() + "\t";
                    arquivoTokens.append(linha);

                }
                linha = dado.getLinha() + "\t" + dado.getColuna() + "\t"
                        + dado.getLexema() + "\t" + dado.getToken().getTipo() + "\n";
                arquivoTabelas.append(linha);
                count++;
            }
            
            arquivoTabelas.close();

            if (tabelaDadosErrados.isEmpty()) {
                linha = "\n\n NENHUM ERRO ENCONTRADO\n";
                arquivoTokens.append(linha);
            } else {
                linha = "\n\n E R R O S\n"
                        + "Linha\tColuna\tErro Encontrado\n";
                arquivoTokens.append(linha);
                for (Dado dado : tabelaDadosErrados) {
                    linha = dado.getLinha() + "\t" + dado.getColuna() + "\t"
                            + dado.getToken().getStringToken() + "\n";
                    arquivoTokens.append(linha);
                }
            }
            

            linha = "\n\nTABELA DE SIMBOLOS\n"
                    + "Indice\tLexema\n";
            arquivoTokens.append(linha);
            for (Simbolo simbolo : tabelaSimbolos) {
                linha = simbolo.getIndice() + "\t" + simbolo.getLexema() + "\n";
                arquivoTokens.append(linha);
            }
            arquivoTokens.close();

        } catch (IOException ex) {
            Logger.getLogger(Imprimir.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

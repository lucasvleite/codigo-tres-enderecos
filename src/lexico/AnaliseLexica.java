package lexico;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Breno Ramos e Lucas Vicente
 */
public class AnaliseLexica {

    // ArraList com as Tabelas solicitadas no trabalho

    static ArrayList<Dado> tabelaDadosErrados = new ArrayList();

    /* Vetor de String com as palavras reservadas */
    static final String[] KEYWORDS = {"abstract", "boolean", "char",
        "class", "else", "extends", "false", "import", "if", "instanceof", "int",
        "new", "null", "package", "private", "protected", "public", "return",
        "static", "super", "this", "true", "void", "while"};

    /* Vetor de Caracteres com os separadores */
    static final char[] SEPARATORS = {',', '.', '[', '{', '(', ')', '}', ']', ';'};

    /* Vetor de Cacacteres com os operadores*/
    static final String[] OPERATORS = {"=", ">", "!", "-", "+", "*", "==", "++", "&&", "<=", "--", "+="};

    /**
     * Função que verifica se o lexema encontrado é uma palavra reservada
     *
     * @param lexema
     * @return true caso seja uma palavra reservada ou false caso não seja
     */
    public static boolean isKeyWord(String lexema) {
        for (String aux : KEYWORDS) {
            if (aux.equals(lexema)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Função que verifica se o caractere encontrado é um separador
     *
     * @param ch
     * @return true caso seja um separador ou false caso não seja
     */
    public static boolean isSeparator(char ch) {
        for (char aux : SEPARATORS) {
            if (aux == ch) {
                return true;
            }
        }
        return false;
    }

    /**
     * Função que verifica se o caractere encontrado é um operador
     *
     * @param operator string com o operador
     * @return true caso seja um operador ou false caso não seja
     */
    public static boolean isOperator(String operator) {
        for (String aux : OPERATORS) {
            if (aux.equals(operator)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Função que verifica se os dois caracteres encontrados é um comentário //
     *
     * @param aux string de dois caracteres para verificar se é igual a //
     * @return true os dois são //, portanto comentário, ou false caso não sejam
     */
    public static boolean isComment(String aux) {
        return (aux.equals("//"));
    }

    /**
     * Função que verifica se o caractere é uma aspas simples para analisar
     *
     * @param buffer
     * @param coluna
     * @return true caso encontre a próxima aspas simples ou false caso não ache
     */
    public static boolean isCharacter(char[] buffer, int coluna) {
        for (int i = coluna; i < buffer.length; i++) {
            if (buffer[i] == '\'') {
                return true;
            }
        }
        return false;
    }

    /**
     * Função que verifica se o caractere é uma aspas dupla para analisar
     *
     * @param buffer
     * @param coluna
     * @return true caso encontre a próxima aspas dupla ou false caso não ache
     */
    public static boolean isString(char[] buffer, int coluna) {
        for (int i = coluna; i < buffer.length; i++) {
            if (buffer[i] == '\"') {
                return true;
            }
        }
        return false;
    }

    /**
     * Função que verifica se o lexema está na tabela de simbolos e retorna a
     * po- sição encontrada, caso elemento não esteja na tabela a função
     * adiciona o lexema
     *
     * @param lexema
     * @param tabelaSimbolos
     * @return um valor inteiro indicando o indice do lexema na tabela de
     * Simbolos
     */
    public static int isSymbolTable(String lexema, ArrayList<Simbolo> tabelaSimbolos) {
        int contador = 0;

        for (Simbolo aux : tabelaSimbolos) {
            contador++;
            if (aux.getLexema().equals(lexema)) {
                return aux.getIndice();
            }
        }
        Simbolo symbol = new Simbolo(contador, lexema);
        tabelaSimbolos.add(symbol);
        return symbol.getIndice();
    }

    public static void addError(char ch, long numeroLinha, int coluna) {
        Dado objDado = new Dado(numeroLinha, coluna + 1);
        objDado.setLexema(Character.toString(ch));

        Token objToken = new Token("Error", Character.toString(ch));
        objDado.setToken(objToken);
        tabelaDadosErrados.add(objDado);
    }

    public static void addError(char[] buffer, long numeroLinha, int coluna) {
        String erro = "";
        for (int i = coluna; i < buffer.length; i++) {
            erro = erro.concat(Character.toString(buffer[i]));
        }
        Dado objDado = new Dado(numeroLinha, coluna + 1);
        objDado.setLexema((erro));

        Token objToken = new Token("Error", (erro));
        objDado.setToken(objToken);
        tabelaDadosErrados.add(objDado);
    }

    /**
     * Contrutor da classe que efetua todo processamento do código e chama a
     * classe Imprimir para mostrar os resultados
     *
     * @param codigo
     * @param tabelaDadosCertos
     * @param tabelaSimbolos
     */
    public AnaliseLexica(BufferedReader codigo,  ArrayList<Dado> tabelaDadosCertos, ArrayList<Simbolo> tabelaSimbolos) {
        long numeroLinha = 0;
        String linha;

        try {
            while ((linha = codigo.readLine()) != null) {
                numeroLinha++;
                char[] buffer = linha.toCharArray();
                int coluna = 0;

                while (coluna < buffer.length) {
                    String lexema = "";
                    char ch = buffer[coluna];

                    if ((Character.isSpaceChar(ch)) || Character.isWhitespace(ch)
                            || ch == '\t') {
                        coluna++;

                    } else if (ch == '/') {
                        if (coluna + 1 == buffer.length) {
                            addError(ch, numeroLinha, coluna);
                        } else if (buffer[coluna + 1] == '/') {
                            coluna = buffer.length;
                        } else {
                            addError(ch, numeroLinha, coluna);
                        }
                        coluna++;

                    } else if (Character.isLetter(ch)) {
                        Dado objDado = new Dado(numeroLinha, coluna + 1);

                        while (Character.isLetterOrDigit(ch)) {
                            lexema = lexema.concat(Character.toString(ch));
                            coluna++;
                            if (coluna == buffer.length) {
                                ch = ' ';
                            } else {
                                ch = buffer[coluna];
                            }
                        }

                        objDado.setLexema(lexema);
                        int indice = isSymbolTable(lexema, tabelaSimbolos);
                        Token objToken;

                        if (isKeyWord(lexema)) {
                            objToken = new Token("reserved_word", Integer.toString(indice));
                        } else {
                            objToken = new Token("id", Integer.toString(indice));
                        }
                        objDado.setToken(objToken);
                        tabelaDadosCertos.add(objDado);

                    } else if (Character.isDigit(ch)) {
                        Dado objDado = new Dado(numeroLinha, coluna + 1);

                        while (Character.isDigit(ch)) {
                            lexema = lexema.concat(Character.toString(ch));
                            coluna++;
                            if (coluna == buffer.length) {
                                ch = ' ';
                            } else {
                                ch = buffer[coluna];
                            }
                        }

                        lexema = "\"" + lexema + "\"";
                        objDado.setLexema(lexema);
                        Token objToken = new Token("Integer", lexema);
                        objDado.setToken(objToken);
                        tabelaDadosCertos.add(objDado);

                    } else if (isSeparator(ch)) {
                        Dado objDado = new Dado(numeroLinha, coluna + 1);
                        lexema = lexema.concat(Character.toString(ch));
                        objDado.setLexema(lexema);

                        Token objToken = new Token("separator", lexema);
                        objDado.setToken(objToken);
                        tabelaDadosCertos.add(objDado);
                        coluna++;

                    } else if (ch == '\'') {
                        if (coluna + 1 == buffer.length) {
                            addError(ch, numeroLinha, coluna);
                        } else if (!isCharacter(buffer, coluna + 1)) {
                            addError(buffer, numeroLinha, coluna);
                            coluna = buffer.length;
                        } else {
                            Dado objDado = new Dado(numeroLinha, coluna + 1);

                            coluna++;
                            ch = buffer[coluna];
                            while (ch != '\'') {
                                lexema = lexema.concat(Character.toString(ch));
                                coluna++;
                                ch = buffer[coluna];
                            }

                            lexema = "\'" + lexema + "\'";

                            objDado.setLexema(lexema);
                            Token objToken = new Token("Character", (lexema));
                            objDado.setToken(objToken);
                            tabelaDadosCertos.add(objDado);
                        }
                        coluna++;

                    } else if (ch == '\"') {
                        if (coluna + 1 == buffer.length) {
                            addError(ch, numeroLinha, coluna);
                        } else if (!isString(buffer, coluna + 1)) {
                            addError(buffer, numeroLinha, coluna);
                            coluna = buffer.length;
                        } else {
                            Dado objDado = new Dado(numeroLinha, coluna + 1);

                            coluna++;
                            ch = buffer[coluna];
                            while (ch != '\"') {
                                lexema = lexema.concat(Character.toString(ch));
                                coluna++;
                                ch = buffer[coluna];
                            }

                            lexema = "\"" + lexema + "\"";

                            objDado.setLexema(lexema);
                            Token objToken = new Token("String", (lexema));
                            objDado.setToken(objToken);
                            tabelaDadosCertos.add(objDado);
                        }
                        coluna++;

                    } else if (ch == '=' || ch == '>' || ch == '!' || ch == '-'
                            || ch == '+' || ch == '*' || ch == '&' || ch == '<') {
                        if (buffer.length > coluna + 1) {
                            if (isOperator(Character.toString(ch) + Character.toString(buffer[coluna + 1]))) {
                                Dado objDado = new Dado(numeroLinha, coluna + 1);
                                lexema = lexema.concat(Character.toString(ch) + Character.toString(buffer[coluna + 1]));
                                objDado.setLexema(lexema);
                                Token objToken = new Token("operator", lexema);
                                objDado.setToken(objToken);
                                tabelaDadosCertos.add(objDado);
                                coluna++;
                            } else if (isOperator(Character.toString(ch))) {
                                Dado objDado = new Dado(numeroLinha, coluna + 1);
                                lexema = lexema.concat(Character.toString(ch));
                                objDado.setLexema(lexema);
                                Token objToken = new Token("operator", lexema);
                                objDado.setToken(objToken);
                                tabelaDadosCertos.add(objDado);
                            } else {
                                addError(ch, numeroLinha, coluna);
                            }
                        } else if (isOperator(Character.toString(ch))) {
                            Dado objDado = new Dado(numeroLinha, coluna + 1);
                            lexema = lexema.concat(Character.toString(ch));
                            objDado.setLexema(lexema);
                            Token objToken = new Token("operator", lexema);
                            objDado.setToken(objToken);
                            tabelaDadosCertos.add(objDado);
                        } else {
                            addError(ch, numeroLinha, coluna);
                        }
                        coluna++;

                    } else {
                        addError(ch, numeroLinha, coluna);
                        coluna++;
                    }
                }

            }
        } catch (IOException ex) {
            Logger.getLogger(AnaliseLexica.class.getName()).log(Level.SEVERE, null, ex);
        }

        
    }
}

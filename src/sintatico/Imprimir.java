package sintatico;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import lexico.Dado;
import lexico.Token;

/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class Imprimir {

    static ArrayList<Dado> tabelaDadosErrados = new ArrayList();
    //ArrayList<Token> tabelaTokenEncontrado = null;
    
    Imprimir(Dado dado){
        Imprimir.tabelaDadosErrados.add(dado);
        //this.tabelaTokenEncontrado.add(encontrado);
    }

    Imprimir(Dado dado, String funcaoErro){
        Token novoToken = new Token(dado.getToken().getTipo() + funcaoErro, dado.getToken().getValor());
        Dado novoDado = new Dado(dado.getLinha(), dado.getColuna(), dado.getLexema(), novoToken);
        Imprimir.tabelaDadosErrados.add(novoDado);
    }

    Imprimir() {
        try (BufferedWriter arquivoTokensErrados = new BufferedWriter(new FileWriter("ErrosSintaxe.csv"))) {
            String linha;

            if (tabelaDadosErrados.isEmpty()) {
                linha = "\n NENHUM ERRO ENCONTRADO\n";
                arquivoTokensErrados.append(linha);
            } else {
                linha = "\n E R R O S\n"
                        + "Linha\tColuna\tLexema\tTipo\n";
                arquivoTokensErrados.append(linha);

                for (Dado dado : tabelaDadosErrados) {

                    linha = dado.getLinha() + "\t" + dado.getColuna() + "\t" +
                            dado.getToken().getValor() + "\t" +
                            dado.getToken().getTipo()+"\n";
                    arquivoTokensErrados.append(linha);

                }
            }
            arquivoTokensErrados.close();
        } catch (IOException ex) {
            Logger.getLogger(Imprimir.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

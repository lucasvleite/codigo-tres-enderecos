package sintatico;

import java.util.ArrayList;
import lexico.Dado;
import lexico.Simbolo;
import lexico.Token;
import sintatico.ArrumandoPilha;

/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class AnaliseSintatica {

    private final ArrayList<Dado> listaDados;
    private final ArrayList<Simbolo> tabelaSimbolos;

    static ArrayList<Token> pilha = new ArrayList();

    int posicaoToken = -1;
    int variavel = 0;

    static final String[] MODIFIERS = { "public", "protected", "private", "static", "abstract" };
    static final String[] BASICTYPE = { "boolean", "char", "int" };


    Token la(int pegarToken) {
        //Verifica se o proximo token nao ultrapassa o ultimo
        if (posicaoToken + pegarToken < listaDados.size()) {
            //Verifica se é identificador ou palavra reservada, pois se for eles armazenam o endereco da tabela de símbolos... e não o lexema
            if("id".equals(listaDados.get(posicaoToken + pegarToken).getToken().getTipo())
            || "reserved_word".equals(listaDados.get(posicaoToken + pegarToken).getToken().getTipo())) {
                //Pego o indice da tabela de simbolos para achar o lexema real
                int posicaoTabelaSimbolo = Integer.parseInt(listaDados.get(posicaoToken + pegarToken).getToken().getValor());

                Token novo = 
                    new Token(listaDados.get(posicaoToken + pegarToken).getToken().getTipo(),
                        tabelaSimbolos.get(posicaoTabelaSimbolo).getLexema() );
                return novo;
            }

            return listaDados.get(posicaoToken + pegarToken).getToken();
        }
        return null;
    }

    public void match(Token token) {
        // pilha.add(token);
        posicaoToken += 1;
    }

    boolean isModifiers(Token token) {
        for (String aux : MODIFIERS) {
            if (aux.equals(token.getValor())) {
                return true;
            }
        }
        return false;
    }

    boolean isBasicType(Token token) {
        for (String aux : BASICTYPE) {
            if (aux.equals(token.getValor())) {
                return true;
            }
        }
        return false;
    }

    boolean isLiteral(Token token) {
        return ("Integer".equals(token.getTipo()) || "Character".equals(token.getTipo())
                || "String".equals(token.getTipo()) || "true".equals(la(1).getValor())
                || "false".equals(la(1).getValor()) || "null".equals(la(1).getValor()));
    }

    boolean firstPrimary(Token token) {
        return ("(".equals(token.getValor()) || "this".equals(token.getValor()) || "super".equals(token.getValor())
                || isLiteral(token) || "new".equals(token.getValor()) || "id".equals(token.getTipo()));
    }

    boolean firstSimpleUnaryExpression(Token token) {
        return ("!".equals(token.getValor()) || firstPrimary(token));
    }

    boolean firstExpression(Token token) {
        return ("++".equals(token.getValor()) || "-".equals(token.getValor()) || firstSimpleUnaryExpression(token));
    }
    
    public void VerificarLocalOrStatement(){
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
            while (".".equals(la(1).getValor()) && "id".equals(la(2).getTipo())) {
                match(la(1));
                match(la(1));
            }
        }
    }

    private void compilationUnit() {
        if (la(1).getValor().equals("package")) {
            match(la(1));
            qualifiedIdentifier();
            if (la(1).getValor().equals(";")) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
        while ("import".equals(la(1).getValor())) {
            match(la(1));
            qualifiedIdentifier();
            if (la(1).getValor().equals(";")) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
        while (posicaoToken < listaDados.size() - 1) {
            if (isModifiers(la(1))) {
                typeDeclaration();
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
    }

    public void qualifiedIdentifier() {
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
            while (".".equals(la(1).getValor()) && "id".equals(la(2).getTipo())) {
                match(la(1));
                match(la(1));
            }
            if (".".equals(la(1).getValor())) {
                match(la(1));
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void typeDeclaration() {
        modifiers();
        classDeclaration();
    }

    public void modifiers() {
        if (isModifiers(la(1))) {
            match(la(1));
            while (isModifiers(la(1))) {
                match(la(1));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void classDeclaration() {
        if ("class".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
                if ("extends".equals(la(1).getValor())) {
                    match(la(1));
                    qualifiedIdentifier();
                }
                classBody();
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void classBody() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));
            while (isModifiers(la(1))) {
                modifiers();
                memberDecl();
            }
            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void memberDecl() {
        if (("void").equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
                formalParameters();
                while (";".equals(la(1).getValor()) || "{".equals(la(1).getValor())) {
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    } else if ("{".equals(la(1).getValor())) {
                        block();
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else if ("id".equals(la(1).getTipo()) && "(".equals(la(2).getValor())) {
            match(la(1));
            formalParameters();
            block();
        } else if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            if ("id".equals(la(1).getTipo())) {
                if ("(".equals(la(2).getValor())) {
                    match(la(1));
                    formalParameters();
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else if ("{".equals(la(1).getValor())) {
                        block();
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                } else {
                    variableDeclarators();
                    if (";".equals(la(1).getValor())) {
                        match(la(1));
                    } else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void block() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));
            while ("{".equals(la(1).getValor()) || "id".equals(la(1).getTipo()) || "if".equals(la(1).getValor())
                    || "while".equals(la(1).getValor()) || "return".equals(la(1).getValor())
                    || ";".equals(la(1).getValor()) || firstExpression(la(1)) || isBasicType(la(1))) {
                blockStatement();
            }

            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void blockStatement() {
        if(isBasicType(la(1))
        || ("id".equals(la(1).getTipo()) && "[".equals(la(2).getValor()))
        || ("id".equals(la(1).getTipo()) && ".".equals(la(2).getValor()))
        || ("id".equals(la(1).getTipo()) && "id".equals(la(2).getTipo()))) {
            int rollBack = posicaoToken;

            VerificarLocalOrStatement();
            
            if("(".equals(la(1).getValor())){
                posicaoToken = rollBack;
                statement();
            }
            else {
                posicaoToken = rollBack;
                localVariableDeclarationStatement();
            }
        }
        else if ("{".equals(la(1).getValor()) || ("id".equals(la(1).getTipo()) && ":".equals(la(2).getValor()))
        || "if".equals(la(1).getValor()) || "while".equals(la(1).getValor()) || "return".equals(la(1).getValor())
        || ";".equals(la(1).getValor()) || firstExpression(la(1))) {
            statement();
        }
        else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void statement() {
        if ("{".equals(la(1).getValor())) {
            block();
        }
        else if ("id".equals(la(1).getTipo()) && ":".equals(la(1).getValor())) {
            match(la(1));
            match(la(1));    
            statement();
        }
        else if ("if".equals(la(1).getValor())) {
            match(la(1));
            parExpression();
            statement();
            if ("else".equals(la(1).getValor())) {
                match(la(1));
                statement();
            }
        }
        else if ("while".equals(la(1).getValor())) {
            match(la(1));
            parExpression();
            statement();
        }
        else if ("return".equals(la(1).getValor())) {
            match(la(1));

            if(firstExpression(la(1))) {
                expression();
            }
            if (";".equals(la(1).getValor())) {
                match(la(1));
            }
            else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
        else if (";".equals(la(1).getValor())) {
            match(la(1));
        }
        else {
pilha.add(listaDados.get(posicaoToken).getToken());
            statementExpression();
            if (";".equals(la(1).getValor())) {
                match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
    }

    public void formalParameters() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
                formalParameter();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    formalParameter();
                }
            }
            if(")".equals(la(1).getValor())){
                match(la(1));
            }
            else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void formalParameter() {
        if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            if ("id".equals(la(1).getTipo())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void parExpression() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if (")".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void localVariableDeclarationStatement() {
        if ("id".equals(la(1).getTipo()) || isBasicType(la(1))) {
            type();
            variableDeclarators();
            if (";".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void variableDeclarators() {
        if ("id".equals(la(1).getTipo())) {
            variableDeclarator();
            while (",".equals(la(1).getValor())) {
                match(la(1));
                if ("id".equals(la(1).getTipo())) {
                    variableDeclarator();
                } else {
                    match(la(1));
                    new Imprimir(listaDados.get(posicaoToken));
                }
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void variableDeclarator() {
        if ("id".equals(la(1).getTipo())) {
            match(la(1));
Token aux = (listaDados.get(posicaoToken).getToken());
            while ("=".equals(la(1).getValor())) {
                match(la(1));
                variableInitializer();
            }
pilha.add(aux);
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void variableInitializer() {
        if ("{".equals(la(1).getValor())) {
            arrayInitializer();
        } else if (firstExpression(la(1))) {
            expression();
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void arrayInitializer() {
        if ("{".equals(la(1).getValor())) {
            match(la(1));

            if (firstExpression(la(1)) || "{".equals(la(1).getValor())) {
                variableInitializer();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    variableInitializer();
                }
            }

            if ("}".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void arguments() {
        if ("(".equals(la(1).getValor())) {
            match(la(1));

            if (firstExpression(la(1))) {
                expression();
                while (",".equals(la(1).getValor())) {
                    match(la(1));
                    expression();
                }
            }

            if (")".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void type() {
        if ("id".equals(la(1).getTipo())) {
            referenceType();
        }
        else if (isBasicType(la(1))) {
            if ("[".equals(la(2).getValor())) {
                referenceType();
            } else {
                match(la(1));
            }
        }
        else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void basicType() {
        if (isBasicType(la(1))) {
            match(la(1));
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void referenceType() {
        if (isBasicType(la(1))) {
            basicType();
            if ("[".equals(la(1).getValor())) {
                match(la(1));
                if ("]".equals(la(1).getValor())) {
                    match(la(1));
                    while ("[".equals(la(1).getValor()) && "]".equals(la(2).getValor())) {
                        match(la(1));
                        match(la(1));
                    }
                    if ("[".equals(la(1).getValor())) {
                        match(la(1));
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                } else {
                    match(la(1));
                    new Imprimir(listaDados.get(posicaoToken));
                }
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else if ("id".equals(la(1).getTipo())) {
            qualifiedIdentifier();
            while ("[".equals(la(1).getValor()) && "]".equals(la(2).getValor())) {
                match(la(1));
                match(la(1));
            }
            if ("[".equals(la(1).getValor())) {
                match(la(1));
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void statementExpression() {
        expression();
    }

    public void expression() {
        assignmentExpression();
    }

    public void assignmentExpression() {
        conditionalAndExpression();
        if ("=".equals(la(1).getValor()) || "+=".equals(la(1).getValor())) {
            match(la(1));
            assignmentExpression();
        }
    }

    public void conditionalAndExpression() {
        equalityExpression();
        while ("&&".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            equalityExpression();
        }
    }

    public void equalityExpression() {
        relationalExpression();
        while ("==".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            relationalExpression();
        }
    }

    public void relationalExpression() {
        additiveExpression();

        if (">".equals(la(1).getValor()) || "<=".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            additiveExpression();
        } else if ("instanceof".equals(la(1).getValor())) {
            match(la(1));
            referenceType();
        }
    }

    public void additiveExpression() {
        multiplicativeExpression();
        while ("+".equals(la(1).getValor()) || "-".equals(la(1).getValor())) {
            match(la(1));
Token aux = (listaDados.get(posicaoToken).getToken());
            multiplicativeExpression();
pilha.add(aux);
        }
    }

    public void multiplicativeExpression() {
        unaryExpression();
        while ("*".equals(la(1).getValor())) {
            match(la(1));
Token aux = (listaDados.get(posicaoToken).getToken());
            unaryExpression();
pilha.add(aux);
        }
    }

    public void unaryExpression() {
        if ("++".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            unaryExpression();
        } else if ("-".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            unaryExpression();
        }
        else if(firstSimpleUnaryExpression(la(1))) {
            simpleUnaryExpression();
        }
        else{
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void simpleUnaryExpression() {
        if ("!".equals(la(1).getValor())) {
            match(la(1));
            unaryExpression();
        } else if ("(".equals(la(1).getValor())) {
            match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
            if (isBasicType(la(1))) {
                if (")".equals(la(2).getValor())) {
                    basicType();
                    match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
                } else {
                    referenceType();
                    if (")".equals(la(1).getValor())) {
                        match(la(1));
pilha.add(listaDados.get(posicaoToken).getToken());
                        unaryExpression();
                    } else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                }
            } else if ("id".equals(la(1).getTipo())) {
                referenceType();
                if (")".equals(la(1).getValor())) {
                    match(la(1));
Token aux = (listaDados.get(posicaoToken).getToken());
                    unaryExpression();
pilha.add(aux);
                } else {
                    match(la(1));
                    new Imprimir(listaDados.get(posicaoToken));
                }
            }
        } else if (firstPrimary(la(1))) {
            postfixExpression();
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void postfixExpression() {
        primary();
        while (".".equals(la(1).getValor()) || "[".equals(la(1).getValor())) {
            selector();
        }
        while ("--".equals(la(1).getValor())) {
            match(la(1));
        }
    }

    public void selector() {
        if (".".equals(la(1).getValor())) {
            match(la(1));
            if ("id".equals(la(1).getTipo())) {
                qualifiedIdentifier();
                if ("(".equals(la(1).getValor())) {
                    arguments();
                }
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else if ("[".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if ("]".equals(la(1).getValor())) {
                match(la(1));
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void primary() {
        if ("(".equals(la(1).getValor())) {
            parExpression();
        } else if ("this".equals(la(1).getValor())) {
            match(la(1));
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
        } else if ("super".equals(la(1).getValor())) {
            match(la(1));
            if ("(".equals(la(1).getValor())) {
                arguments();
            } else if (".".equals(la(1).getValor())) {
                match(la(1));
                if ("id".equals(la(1).getTipo())) {
                    match(la(1));
                    if ("(".equals(la(1).getValor())) {
                        arguments();
                    }
                }
            } else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        } else if (isLiteral(la(1))) {
            literal();
        } else if ("new".equals(la(1).getValor())) {
            match(la(1));
            creator();
        } else if ("id".equals(la(1).getTipo())) {
            qualifiedIdentifier();
// pilha.add(listaDados.get(posicaoToken).getToken());
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void creator() {
        if (isBasicType(la(1)) || "id".equals(la(1).getTipo())) {
            if ("id".equals(la(1).getTipo())) {
                qualifiedIdentifier();
            } else
                match(la(1));
            
            if ("(".equals(la(1).getValor())) {
                arguments();
            }
            else if("[".equals(la(1).getValor()) && firstExpression(la(2))){
                newArrayDeclarator();
            }
            else {
                if("[".equals(la(1).getValor())) {
                    match(la(1));
                    if("]".equals(la(1).getValor())) {
                        match(la(1));
                        while("[".equals(la(1).getValor())) {
                            match(la(1));
                            if("]".equals(la(1).getValor())) {
                                match(la(1));
                            }
                            else {
                                match(la(1));
                                new Imprimir(listaDados.get(posicaoToken));
                            }
                        }
                        if("{".equals(la(1).getValor())) {
                            arrayInitializer();
                        }
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                }
                else {
                    match(la(1));
                    new Imprimir(listaDados.get(posicaoToken));
                }
            }
        } else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void newArrayDeclarator() {
        if ("[".equals(la(1).getValor())) {
            match(la(1));
            expression();
            if ("]".equals(la(1).getValor())) {
                match(la(1));
                while ("[".equals(la(1).getValor()) && firstExpression(la(2))) {
                    match(la(1));
                    expression();
                    if ("]".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                }
                while ("[".equals(la(1).getValor())) {
                    match(la(1));
                    if ("]".equals(la(1).getValor())) {
                        match(la(1));
                    }
                    else {
                        match(la(1));
                        new Imprimir(listaDados.get(posicaoToken));
                    }
                }
            }
            else {
                match(la(1));
                new Imprimir(listaDados.get(posicaoToken));
            }
        }
        else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }

    public void literal() {
        if (isLiteral(la(1))) {
            match(la(1));
            if("Integer".equals(listaDados.get(posicaoToken).getToken().getTipo())) {
pilha.add(listaDados.get(posicaoToken).getToken());
            }
        }
        else {
            match(la(1));
            new Imprimir(listaDados.get(posicaoToken));
        }
    }


    /*
     * Construtor
    */
    public AnaliseSintatica(ArrayList<Dado> tabelaTokens, ArrayList<Simbolo> tabelaSimbolos) {
        this.listaDados = tabelaTokens;
        this.tabelaSimbolos = tabelaSimbolos;

        ArrumandoPilha codigo = new ArrumandoPilha(tabelaSimbolos);
        
        compilationUnit();
        pilha.forEach((aux) -> {
            System.out.println(aux.getValor());
        });

        codigo.ArrumandoPilha();

        new Imprimir();
    }

}

package sintatico;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;
import lexico.Simbolo;

import lexico.Token;

/**
 *
 * @author
 *  Breno Ramos
 *  Lucas Vicente
 *  Marina Alcântara
 */
public class ArrumandoPilha {
    
    static ArrayList<String> todoCodigo = new ArrayList();
    private ArrayList<Simbolo> tabelaSimbolos = new ArrayList<>();

    public ArrumandoPilha(ArrayList<Simbolo> tabelaSimbolos) {
        this.tabelaSimbolos = tabelaSimbolos;
    }
    
    /**
     *
     * @param pilha
     */
    public void GerandoCodigo(ArrayList<Token> pilha) {
        Stack pilhaAux = new Stack();
        String linha, Operando2;
        int valorT = 1;
        
        
        
//        while( i<pilha.size()) {
//            aux = pilha.get(i);
//            if("operator".equals(aux.getTipo())) {
//
//            }
//            if("id".equals(aux.getTipo())) {
//                linha = "\nv" + aux.getValor() + " = t" + valorT;
//                System.out.println(linha);
//                todoCodigo.add(linha);
//                valorT++;
//                i++;
//            }
//            else {
//                linha = "t"+ valorT + aux.get(i).getValor() + " " aux.get(i+2).getValor() + " " aux.get(i+1).getValor() + " ";
//                todoCodigo.add(linha);
//                valorT++;
//                i += 3;
//            }
//        }
//        
    }

    
    public void ArrumandoPilha() {
        try (BufferedWriter arquivo = new BufferedWriter(new FileWriter("Codigo.txt"))) {
            String linha;

            if (todoCodigo.isEmpty()) {
                linha = "\n\n NENHUM CODIGO GERADO\n";
                arquivo.append(linha);
            } else {
                linha = "\n\n CODIGO(S)\n";
                arquivo.append(linha);
                for (String dado : todoCodigo) {
                    linha = dado + "\n";
                    arquivo.append(linha);
                }
            }


            linha = "\n\nTABELA DE SIMBOLOS\n"
                    + "Indice\tLexema\n";
            arquivo.append(linha);
            for (Simbolo simbolo : tabelaSimbolos) {
                linha = simbolo.getIndice() + "\t" + simbolo.getLexema() + "\n";
                arquivo.append(linha);
            }
            arquivo.close();
        }
        catch (IOException ex) {
            Logger.getLogger(ArrumandoPilha.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}